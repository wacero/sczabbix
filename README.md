SCZABBIX
========

SCZABBIX is a python module used to send quality control metrics from seiscomp's SCQC to a zabbix server to ease the visualization of QC metrics, generate automatic alerts and detect posible problems with the stations.

# Requirements 

Install the library PY-ZABBIX

```
$ python3 -m pip install --user py-zabbix  

```

# Download and install

Clone the repository and create a tar.gz file
```
$ cd /tmp 
$ git clone https://gitlab.com/wacero/sczabbix.git
$ cd sczabbix/
$ ln -s /tmp/sczabbix/bin/sczabbix.py bin/sczabbix
$ tar -hzcvf seiscomp-sczabbix.tar.gz -T tar_list.txt --transform 's,^,seiscomp/,'
```

# Install into SeisComP 

```
$ cd /tmp/sczabbix
$ tar zxfv seiscomp-sczabbix.tar.gz -C $HOME 
```

# Configure SCZABBIX

Change the parameters zabbixAddress, zabbixPort and networkID according to your requirements. 

![This is an image](/images/sczabbix_config.png)


# Enable and run the module

Once installed and configured the module should start capturing QC messages from the networkID and send them as zabbix traps to a zabbix server. 

```
$ seiscomp enable sczabbix
$ seiscomp start sczabbix 
```

# Configure Zabbix 

To visualize the QC metrics sent by seiscomp, you must create your hosts in Zabbix. This process can be automated using Zabbix API. 

A zabbix template is included with items, triggers and graphs, as you can see in the following images. 

![This is an image](/images/sczabbix_items.png)
![This is an image](/images/sczabbix_triggers.png)
![This is an image](/images/sczabbix_graphs.png)

## Important
Change the parameter ALLOWED HOSTS in the template, this IP address corresponds to the SeisComP server(s) that runs SCZABBIX. This paremeter accepts multiple IPs. 

![This is an image](/images/sczabbix_allowed_hosts.png)


# Final results

Once you have completed the instalation and configuration. The results can be checked individually or using Zabbix Dashboard. 

## Zabbix Dashboard

![This is an image](/images/sczabbix_dashboard.png)

## Timing problem 

![This is an image](/images/sczabbix_timing.png)

## Data availability 

![This is an image](/images/sczabbix_availability.png)
